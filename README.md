# Lab 2. Denial of Service by Deserialization

This repository demonstrates the attack on Java serialization. It contains a simplified application that allows users serialize data about planets and upload the serialized objects back.

There are three "legitimate" planets to download and two "degenerate". The legitimate ones are:

- Mercury
- Venus
- Earth

You can download them for example like this:

``` text
$ java Download earth
Earth:
rO0ABXNyAAZQbGFuZXSgV2hDIHwMJgIABEQABG1hc3NJAAVvcmRlckwABG5hbWV0ABJMamF2YS9sYW5nL1N0cmluZztMAAhuaWNrbmFtZXEAfgABeHA/8AAAAAAAAAAAAAN0AAVFYXJ0aHQAC0JsdWUgcGxhbmV0
```

It is possible to re-upload the serialized data with the following command:

``` text
$ echo rO0ABXNyAAZQbGFuZXSgV2hDIHwMJgIABEQABG1hc3NJAAVvcmRlckwABG5hbWV0ABJMamF2YS9sYW5nL1N0cmluZztMAAhuaWNrbmFtZXEAfgABeHA/8AAAAAAAAAAAAAN0AAVFYXJ0aHQAC0JsdWUgcGxhbmV0 | java Upload
Successfully uploaded:
3. Earth (Blue planet): 1.0
```

For simplicity you can pipe the two commands:

``` text
$ java Download mercury | java Upload
Successfully uploaded:
1. Mercury (Swift planet): 0.0553
```

The `Upload` program is smart enough to ignore the data that doesn't look like serialized objects.

## The attack

The problem is that some serialized objects break the `Upload` program. These are the two degenerate cases. First is relatively benign:

``` text
$ java Download pluto | java Upload
	at Upload.main(Upload.java:27)
Exception in thread "main" java.lang.ClassCastException: class java.lang.String cannot be cast to class Planet (java.lang.String is in module java.base of loader 'bootstrap'; Planet is in unnamed module of loader 'app')
	at Upload.processUpload(Upload.java:13)
```

This is bad enough, but really bad payload is the Black Hole.

``` text
$ java Download black-hole | java Upload
```

Don't hold your breath. This command will never return on its own. You will have to kill it with CTRL-C or something like that. Even worse, it will consume all the CPU it can. Check it out with `top`.

The exact steps of the attack are codified in the [Makefile](./Makefile). 

## Your job

First order of business is to prevent the Black Hole from sucking all resources from our system. Notice that the CI is failing. That's because there are already tests in place to make sure that the DoS attack like this can't happen.

Fork the repository and make the CI pass. Try to avoid modifying anything else than the Java code. Notice that the tests expect specific exception to be thrown if the payload is invalid. Essentially you should only allow serialized instances of the `Planet` class to be uploaded. Everything else should throw an `InvalidClassException` clearly stating that the class is unsupported and the name of the uploaded class. Something that looks like this:

``` text
$ java Download black-hole | java Upload
Exception in thread "main" java.io.InvalidClassException: Unsupported class; java.util.HashSet
	at PlanetInputStream.resolveClass(PlanetInputStream.java:14)
	at java.base/java.io.ObjectInputStream.readNonProxyDesc(ObjectInputStream.java:2005)
	at java.base/java.io.ObjectInputStream.readClassDesc(ObjectInputStream.java:1872)
	at java.base/java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2179)
	at java.base/java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1689)
	at java.base/java.io.ObjectInputStream.readObject(ObjectInputStream.java:495)
	at java.base/java.io.ObjectInputStream.readObject(ObjectInputStream.java:453)
	at Upload.processUpload(Upload.java:13)
	at Upload.main(Upload.java:27)
```

At the same time everything else should continue to work as it does.

This is meant to be a group exercise. Together try to find a most elegant solution.

## Extras

If you were able to fix the Black Hole vulnerability and have appetite for more, then un-comment the `test-pluto` goal in the [Makefile](Makefile#L19) and try to make it pass too. 

## Credit

The code and attack technique is adapted from <https://github.com/kojenov/serial/tree/master/3-4.%20upload>. Most of the credit goes to Alexei Kojenov. We highly recommend watching his presentation about serialization attacks: <https://www.youtube.com/watch?v=t-zVC-CxYjw>
