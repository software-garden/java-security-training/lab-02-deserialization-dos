{
  description = "Java Security - Lab 1. Cookie Serialization";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-compat, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        shared-inputs = [
            pkgs.gnumake
            pkgs.git
            pkgs.jdk
            pkgs.file
            pkgs.unixtools.xxd
            pkgs.dhex
            pkgs.tweak
            pkgs.bsdiff
        ];
      in rec {
        packages.java-security-presentation-serialize-class = pkgs.stdenv.mkDerivation {
          name = "java-security-presentation-serialize-class";
          src = self;
          buildInputs = shared-inputs ++ [
          ];
        };

        devShell = pkgs.mkShell {
          name = "java-security-presentation-serialize-class-development-shell";
          src = self;
          buildInputs = shared-inputs ++ [
            pkgs.cachix
          ];
        };

        defaultPackage = packages.java-security-presentation-serialize-class;
      }
    );
}
