include Makefile.d/defaults.mk

all: ## (Default) Clean and test
all: test dist

### BUILD

dist:
	mkdir --parents $@
	echo "<p>All is well</p>" > $@/index.html

### TEST

test: ## Test if program is working correctly
test: test-mercury
test: test-venus
test: test-earth
test: test-black-hole
# test: test-pluto
	########################################
	#                                      #
	# EVERYTHING IS GOOD! CONGRATULATIONS! #
	#                                      #
	########################################
.PHONY: test

test-mercury: mercury.response
	#
	# ASSERTIONS FOR MERCURY:
	#
	diff - mercury.response << EXPECTED
	Successfully uploaded:
	1. Mercury (Swift planet): 0.0553
	Exit status: 0
	EXPECTED
	#
	# Correct ✅
	#
.PHONY: test-mercury

test-venus: venus.response
	diff - venus.response << EXPECTED
	Successfully uploaded:
	2. Venus (Morning star): 0.815
	Exit status: 0
	EXPECTED
	#
	# Correct ✅
	#
.PHONY: test-venus

test-earth: earth.response
	#
	# ASSERTIONS FOR EARTH:
	#
	diff - earth.response << EXPECTED
	Successfully uploaded:
	3. Earth (Blue planet): 1.0
	Exit status: 0
	EXPECTED
	#
	# Correct ✅
	#
.PHONY: test-earth

test-pluto: pluto.response
	#
	# ASSERTIONS FOR PLUTO
	#
	diff - pluto.response << EXPECTED
	Exception in thread "main" java.io.InvalidClassException: Unsupported class; String
	[ stack trace omitted ]
	Exit status: 1
	EXPECTED
	#
	# Correct ✅
	#
.PHONY: test-pluto

test-black-hole: black-hole.response
	#
	# ASSERTIONS FOR BLACK HOLE:
	#
	diff - black-hole.response << EXPECTED
	Exception in thread "main" java.io.InvalidClassException: Unsupported class; java.util.HashSet
	[ stack trace omitted ]
	Exit status: 1
	EXPECTED
	#
	# Correct ✅
	#
.PHONY: test-black-hole

%.response: $(patsubst %.java, %.class, $(wildcard *.java))
	function handle_exit {
		echo "Exit status: $$?" \
		>> $@

		exit 0
	}

	trap handle_exit exit

# Capture both standard output and standard error, but ommit stack trace

	java Download $* \
	| timeout 5s java Upload \
	2>&1 \
	| sed --regexp-extended 's#^\s+at .+$$#[ stack trace omitted ]#g' \
	| uniq \
	> $@


%.class: %.java
	javac $<


### INSTALL

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install

### DEVELOPMENT

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX
.PHONY: clean

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)
